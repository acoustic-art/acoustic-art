﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
namespace Image_Read
{
    class Image_read
    {
        static void Main(string[] args)
        {

            //cmd line read in image through file

            string imFile = "C:\\Users\\Public\\Pictures\\Sample Pictures\\Jellyfish.jpg";//args[0];
            Bitmap inImage=new Bitmap(imFile);
            Rectangle imRect = new Rectangle(0, 0, inImage.Width, inImage.Height);

            //data
            BitmapData imData = inImage.LockBits(imRect, ImageLockMode.ReadOnly, inImage.PixelFormat);

            //get the first line
            IntPtr top = imData.Scan0;

            //size of our bitmap
            int size = imData.Stride * inImage.Height;
            byte[] rgbBytes = new byte[size];

            //copies from unmanaged array to managed array (yay) (huzzah)
            System.Runtime.InteropServices.Marshal.Copy(top, rgbBytes, 0, size);

            byte[]gray=new byte[size];
            for (int i = 0; i < size; i+=3)
            {
                byte graycol=(byte)(((int)rgbBytes[i]+(int)rgbBytes[i+1]+(int)rgbBytes[i+2])/3);
                gray[i] = gray[i + 1] = gray[i + 2] = graycol;
                
            }


            inImage.UnlockBits(imData);


        }//main
    }//classed
}//spaced
